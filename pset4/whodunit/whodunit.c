#include <stdio.h>

#include "bmp.h"

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		fprintf(stderr,"Error. Programme accepts exactly two command-line arguments. The name of an input file to open for reading followed by the name of an output file to open for writing.\n");
		return 1;
	}
	FILE* inputFile = fopen(argv[1], "r");
	if(inputFile == NULL)
	{
		fprintf(stderr,"Error. Could not open input file.\n");
		return 2;
	}
	FILE* outputFile = fopen(argv[2], "w");
	if(outputFile == NULL)
	{
		fclose(inputFile);
		fprintf(stderr,"Error. Could not create output file.\n");
		return 3;
	}
	BITMAPFILEHEADER bf;
	fread(&bf, sizeof(BITMAPFILEHEADER), 1, inputFile);
	BITMAPINFOHEADER bi;
	fread(&bi, sizeof(BITMAPINFOHEADER), 1, inputFile);
	if(bf.bfType != 0x4d42 || bi.biBitCount != 24 || bi.biCompression != 0 || bi.biSize != 40 || bf.bfOffBits != 54)
	{
		fclose(inputFile);
		fclose(outputFile);
		fprintf(stderr,"Error. File is not a 24-bit uncompressed BMP 4.0.\n");
		return 4;
	}

	// write outfile's BITMAPFILEHEADER
    fwrite(&bf, sizeof(BITMAPFILEHEADER), 1, outputFile);

    // write outfile's BITMAPINFOHEADER
    fwrite(&bi, sizeof(BITMAPINFOHEADER), 1, outputFile);

    // determine padding for scanlines
    int padding =  (4 - (bi.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;

	int i,j,k,biHeight;
    // iterate over infile's scanlines
    for (i = 0, biHeight = abs(bi.biHeight); i < biHeight; i++)
    {
        // iterate over pixels in scanline
        for (j = 0; j < bi.biWidth; j++)
        {
            // temporary storage
            RGBTRIPLE triple;

            // read RGB triple from infile
            fread(&triple, sizeof(RGBTRIPLE), 1, inputFile);

			// filter out red colour
			if(triple.rgbtGreen == 0x00 && triple.rgbtBlue == 0x00) // if only red colour exists change it to white.
			{
				triple.rgbtRed = 0xff;
				triple.rgbtBlue = 0xff;
				triple.rgbtGreen = 0xff;
			}
			if(triple.rgbtRed != 0xff || triple.rgbtGreen != 0xff || triple.rgbtBlue != 0xff) // for all pixels that haven't been changed to white colour by previous if statement change the pixel colour to black.
			{
				triple.rgbtRed = 0x00;
				triple.rgbtBlue = 0x00;
				triple.rgbtGreen = 0x00;
			}

            // write RGB triple to outfile
            fwrite(&triple, sizeof(RGBTRIPLE), 1, outputFile);
        }

        // skip over padding, if any
        fseek(inputFile, padding, SEEK_CUR);

        // then add it back (to demonstrate how)
        for (k = 0; k < padding; k++)
        {
            fputc(0x00, outputFile);
        }
    }

	fclose(inputFile);
	fclose(outputFile);
	return 0;
}
