/**
 * helpers.c
 *
 * Helper functions for Problem Set 3.
 */
 
#include <cs50.h>

#include "helpers.h"

/**
 * Returns true if value is in array of n values, else false.
 */
bool search(int value, int values[], int n) // FUNCTION'S DECLARATION CANNOT BE CHANGED
{
    // TODO: implement a searching algorithm
	// Binary search algorithm is used so that O(N)=LOG(N)
	if(n<=0)
	{
		return false; // RETURN FALSE IMMIDIATE IF N IS NON-POSITIVE
	}
	int right = n-1;
	int left = 0;
	int middle;
	while(right>=left)
	{
		middle = (right - left + 1)%2 ? left+(((right - left + 1)-1)/2) : left+((right - left + 1)/2);
		if(values[middle] == value)
		{
			return true; // TRUE IF VALUE IS IN VALUES
		}
		if(values[middle] > value)
		{
			right = middle-1;
		}
		if(values[middle] < value)
		{
			left = middle+1;
		}
	}
	return false; // FALSE IF VALUE IS NOT IN VALUES
}

/**
 * Sorts array of n values.
 */
void sort(int values[], int n) // FUNCTION DECLARATION CANNOT BE CHANGED
{
    // TODO: implement a sorting algorithm
	// Counting sort algorithm is used - RUNNING TIME OF IMPLEMENTATION O(N)
	int max = 65536; // MAXIMUM VALUE OF RANDOM GENERATED NON-NEGATIVE INTS which are being sorted
	int counting[65536+1] = {0}; // array for counting keys (key is int value). 65536 IS MAXIMUM VALUE OF RANDOM GENERATED NON-NEGATIVE INTS which are being sorted
	int i;
	for(i=0;i<n;i++)
	{
		counting[values[i]] += 1;
	}
	//int sorted[n];
	int j = 0; // j is sorted array index
	for(i=0;i<=max;i++) // SORT FROM SMALLEST TO LARGEST
	{
		while(counting[i]>0) // THERE MAY BE DUPLICATES
		{
			//sorted[j] = i;
			values[j] = i; // sorting values
			counting[i] -= 1;
			j += 1;
		}
	}
    return;
}
