#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(void)
{
	int buffer_size = 80;
	char *name;
	fgets(name, buffer_size, stdin);

	if(name[0]!=' ')
	{
		printf("%c",toupper(name[0]));
	}
	int i, max;
	for(i=0, max=strlen(name); i<=max; i++)
	{
		if(name[i]==' ' && name[i+1]!=' ')
		{
			printf("%c",toupper(name[i+1]));
		}
	}
	printf("\n");
		
	return 0;
}
