#define _XOPEN_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("Error. Program accepts a single command-line argument: a hashed password.\n");
		return 1;
	}
	char salt[3] = {argv[1][0],argv[1][1]};
	int signs = 52; //number of signs user can use in his or her password. 52 for upper and lowercase letters.
	char letters[signs+1],ch;
	for(ch='A';ch<='Z';ch++)
	{
		letters[ch-65] = ch;
	}
	for(ch='a';ch<='z';ch++)
	{
		letters[ch-71] = ch;
	}
	char password[5] = ""; // up to 4 letters password
	int i,j,k,l; // used in for loops
	char *encryption_result;
	for(i=0;i<signs;i++) // check all 1 letter passwords
	{
		password[0] = letters[i];
		encryption_result = crypt(password,salt);
		if(strcmp(argv[1],encryption_result)==0)
		{
			printf("%s\n",password);
			return 0;
		}
	}
	for(i=0;i<signs;i++) // check all 2 letter passwords
	{
		password[0]=letters[i];
		for(j=0;j<signs;j++)
		{
			password[1]=letters[j];
			encryption_result = crypt(password,salt);
			if(strcmp(argv[1],encryption_result)==0)
			{
				printf("%s\n",password);
				return 0;
			}
		}
	}
	for(i=0;i<signs;i++) // check all 3 letter passwords
	{
		password[0]=letters[i];
		for(j=0;j<signs;j++)
		{
			password[1]=letters[j];
			for(k=0;k<signs;k++)
			{
				password[2]=letters[k];
				encryption_result = crypt(password,salt);
				if(strcmp(argv[1],encryption_result)==0)
				{
					printf("%s\n",password);
					return 0;
				}
			}
		}
	}
	for(i=0;i<signs;i++) // check all 4 letter passwords
	{
		password[0]=letters[i];
		for(j=0;j<signs;j++)
		{
			password[1]=letters[j];
			for(k=0;k<signs;k++)
			{
				password[2]=letters[k];
				for(l=0;l<signs;l++)
				{
					password[3]=letters[l];
					encryption_result = crypt(password,salt);
					if(strcmp(argv[1],encryption_result)==0)
					{
						printf("%s\n",password);
						return 0;
					}
				}
			}
		}
	}
	return 0;
}
