#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int check_input(int argc, char* argv[]);

int main(int argc, char* argv[])
{
	if(check_input(argc, argv)) return 1;
	int k = atoi(argv[1]);
	printf("plaintext: ");
	char plaintext[260+1]="";
	fgets(plaintext,260+1,stdin);
	printf("ciphertext: ");
	int length = strlen(plaintext);
	int i;	
	for(i=0;i<length;i++)
	{
		if(isalpha(plaintext[i]))
		{
			if(isupper(plaintext[i]))
			{
				printf("%c",65+(plaintext[i]-65+k)%26);
			}
			if(islower(plaintext[i]))
			{
				printf("%c",97+(plaintext[i]-97+k)%26);
			}
		}
		else
		{
			printf("%c",plaintext[i]);
		}		
	}
	printf("\n");
	return 0;	
}

int check_input(int argc, char* argv[])
{
	if(argc!=2)
	{
		printf("Error. Pass one argument to ./caesar.\n");
		return 1;
	}
	else return 0;
}
